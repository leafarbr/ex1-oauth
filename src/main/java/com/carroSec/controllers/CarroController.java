package com.carroSec.controllers;

import com.carroSec.Carro;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/carro")
public class CarroController {

    @GetMapping("/{carro}")
    Carro consultarCarro(@PathVariable String carro, Principal principal)
    {

        Carro objCarro = new Carro();

        objCarro.setMarca(carro);
        objCarro.setPlaca("ABC1235");
        objCarro.setId_dono(1);
        objCarro.setModelo("Elantra");

        return objCarro;

    }

}
